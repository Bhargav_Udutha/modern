package com.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.service.Calculator;

public class CalculatorTest {
	Calculator  cal=new Calculator();
	@BeforeClass
	public static void init()
	{
		//BeforeClass-logic to be  execute for every Class(once)-but init should be static-same like "AfterClass" but there we use tearDown()
		//Before-logic to be  execute for every test method-same like "After" but there we use tearDown()
		//Calculator cal=new Calculator();
		System.out.println("Executed Before every function");
	}

	@Test
	public void testAdd() {
		//fail("Not yet implemented");
		//Calculator cal=new Calculator();
		int ans1=cal.add(2,3);
		assertEquals(5,ans1);
	}

	@Test
	public void testMul() {
		//fail("Not yet implemented");
		//Calculator cal=new Calculator();
		int ans1=cal.mul(2,3);
		assertEquals(6,ans1);
		
		int ans2=cal.mul(4,3);
		assertEquals(12,ans2);
	}
		
		@Test()//expected=ArithmeticException.class)
		public void testDiv() {
			//Calculator cal=new Calculator();
			int ans1=cal.div(10,2);
			assertEquals(5,ans1);
	}

}
